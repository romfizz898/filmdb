<?php

 /**
 * Build  form.
 */
function reception_page_form($form, $form_state) {
 $form = array();

 $form['name'] = array(
  '#type' => 'textfield',
  '#title' => t('Name'),
  '#required' => TRUE,
  '#size' => 20,
 );

 $form['secondName'] = array(
  '#type' => 'textfield',
  '#title' => t('Second name'),
  '#required' => TRUE,
  '#size' => 20,
 );

 $form['age'] = array(
  '#type' => 'textfield',
  '#title' => t('Age'),
  '#required' => TRUE,
  '#size' => 20,
  '#element_validate' => array('element_validate_integer_positive'), 
 );

 $form['subject'] = array(
  '#type' => 'textfield',
  '#title' => t('Subject'),
  '#required' => TRUE,
  '#size' => 20,
 );

 $form['body'] = array(
  '#type' => 'textarea',
  '#title' => t('Message'),
  '#required' => TRUE,
  '#size' => 100,
 );

 $form['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Send'),
  '#ajax' => array(                         
    'callback' => 'reception_ajax_callback',  
    'wrapper' => 'reception-page-form',             
    ),
 );
 return $form;
}

 /**
 * ajax callback
 */
function reception_ajax_callback ($form, &$form_state) {
  return $form;
}

/** 
 * Validate function for form. 
 */
function reception_page_form_validate($form, &$form_state) {
 if(mb_strlen($form_state['values']['name']) < 3){
    form_set_error('Name', t('Value in field Name is wrong.'));
  }
 if(mb_strlen($form_state['values']['secondName']) < 3) {
    form_set_error('Name', t('Value in field Second Name is wrong.'));
  } 
}

/** 
 * Submit function for form. 
 */
function reception_page_form_submit($form, &$form_state) {
 $user;
 $name = $form_state['values']['name'];
 $secondName = $form_state['values']['secondName'];
 $age = $form_state['values']['age'];
 $subject = $form_state['values']['subject'];
 $body = $form_state['values']['body'];

 $str = $name.'\r\n'. $secondName. '\r\n'. $age. '\r\n'. $body;
 $to =  variable_get('site_mail', ini_get('sendmail_from'));;
 $from = variable_get('site_mail');
 $message = drupal_mail( 'reception' , 'my_notify' , $to , user_preferred_language($user), array('body' => $str, 'subject' => $subject, $from, TRUE));
}
