<?php

/**
 * message_box config form.
 */
function message_box_config_form($form, &$form_state) {

  $form = array();
  
  $form['message_box_width'] = array(
    '#type' => 'textfield',
    '#title' => t('message box width (px)'),
    '#default_value' => variable_get('message_box_width', 3),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );
  
  $form['message_box_header_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('message box header color'),
    '#default_value' => variable_get('message_box_header_color', 3),
    '#required' => TRUE,
  );
  
  $form['message_box_background_color'] = array(
    '#type' => 'jquery_colorpicker',
    '#title' => t('message box background color'),
    '#default_value' => variable_get('message_box_background_color', 3),
    '#required' => TRUE,
  );
   
    drupal_add_js( array (
    'width' => variable_get('message_box_width', 3),
    'header_color' => variable_get('message_box_header_color', 3),
    'back_color' => variable_get('message_box_background_color', 3)
  ),'setting');
  
  drupal_add_js(drupal_get_path('module' , 'message_box') . '/theme/js/set_attr.js');
  
  return system_settings_form($form);
}

function message_box_config_form_validate ($form , $form_state){
  if ( $form_state['values']['message_box_width'] < 50) {
    form_set_error('message_box_width', t('Value in field width is to small.'));
  }  
}
