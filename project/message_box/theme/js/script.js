Drupal.behaviors.popupmessage = {
  attach: function(context, settings) { (function ($) {
     
    $.fn.ShowPopupWindow = function ( id_elem ) {
        
      $('#page').append('<p class="popupwindow" id = "wind" > </p>');
      $('#wind').prepend(' <p class="header" > <a class="close" href="#" title="' + Drupal.t('Close') + '"> <img src= "/' + settings.path + '/theme/images/buttom.png" width = "24" heigth ="20 "> </a> </p>');
      $('#page').prepend('<div id="overlay" > </div>');
           
      if (localStorage.getItem('width')) {
        $('#wind').css('width', localStorage.getItem('width'));
        $('#wind p.header').css('width', localStorage.getItem('width'));
      }
      
      if (localStorage.getItem('header_back')) {
        $('#wind p.header').css('background', '#' + localStorage.getItem('header_back'));
      }
      
      if (localStorage.getItem('wind_back')) {
        $('#wind').css('background', '#' + localStorage.getItem('wind_back'));
      }
        
      var arrayLength = id_elem.length;
      var height = 0;
      for (var i = 0; i < arrayLength; i++) {
        $('#wind').append($('#'+id_elem[i]));
        height += $('#'+id_elem[i]).length;
        $('#'+id_elem[i]).css({
          '-moz-user-select': 'none'
          ,'-o-user-select': 'none'
          ,'-khtml-user-select': 'none'
          ,'-webkit-user-select': 'none'
          ,'-ms-user-select': 'none'
          ,'user-select': 'none'
        });
      }
      
      $('#page').css({
        '-moz-user-select': 'none'
        ,'-o-user-select': 'none'
        ,'-khtml-user-select': 'none'
        ,'-webkit-user-select': 'none'
        ,'-ms-user-select': 'none'
        ,'user-select': 'none'
      });
      
      $('#wind').css('height', height + 150 * arrayLength);
      
      $('#wind').fadeIn(400,
        function(){ 
          $('#wind') 
          .css('display' , 'block') 
          .animate({opacity: 1, top: '50%'}, 200);                
        }); 
        
      return this;  
      
    }
        
    $.fn.closeButtonMessages = function() {  
 
      $('#wind' + ' a.close').click(function(e) {
        e.preventDefault();
        $('#wind').fadeOut('slow');
        $('#overlay').fadeOut('fast');
        $('#page').css({
          '-moz-user-select': 'all'
          ,'-o-user-select': 'all'
          ,'-khtml-user-select': 'all'
          ,'-webkit-user-select': 'all'
          ,'-ms-user-select': 'all'
          ,'user-select': 'all'
        }); 
      });   
    };
      
    $.fn.moviePopupWindow = function() {
      var isDown = false;
      $('#wind p.header').mousedown(function(e) { 
        isDown = true; 
        $('#wind p.header').mousemove( function(e) {
          if (isDown === true) { 
            var offset1 = $('#wind').offset();
            var offset2 = $('#page').offset();      
            $( '#wind' ).css('top', e.pageY - offset1.top/2 + (offset1.top - offset2.top)/2 + 'px' );
            $( '#wind' ).css('left',e.pageX - offset1.left/2 + 'px' );
          }
        });
         
        $('#wind p.header').mouseup(function(e) {
          isDown = false;
        })
        
        $('#wind p.header').mouseout(function(e) {
          isDown = false;
        })          
      });
    };
     
    $.fn.Popupmessage = function (){ 
      var i=1;
      var id_elem = [];
      var flag = false;
      
      $('.messages').each(function() {
        flag = true;  
        $(this).attr('id',i);
        id_elem.push(i);
        i+=1; 
      })
      
      if (flag === true){
        $().ShowPopupWindow(id_elem);
        $().closeButtonMessages();
        $().moviePopupWindow();  
      }  
    }
    
    $().Popupmessage();
    
  })(jQuery);
  }
}

