<?php

require_once( drupal_get_path('module', 'dynamic_filtres'). '/model/dynamic_filtres.db.inc');

/**
 * dynamic_filtres config form.
 */
function dynamic_filtres_config_form($form, &$form_state) {
  $allowed_categories = dynamic_filtres_db_product_find_categories ();
  $form = array();
  drupal_add_css(drupal_get_path('module', 'dynamic_filtres'). '/theme/css/config.css');

  $form['mades'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mades'),
    '#collapsed' => FALSE,  
  );

  $form['mades']['dynamic_filtres_mades_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Mades of product name'),
    '#default_value' => variable_get('dynamic_filtres_mades_name'),
    '#required' => TRUE,
    '#size' => '15'
  );

  $form['mades']['dynamic_filtres_mades'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('dynamic_filtres_mades'),
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#options' => $allowed_categories,
    '#size' => '15'
  );

  $form['brew'] = array(
    '#type' => 'fieldset',
    '#title' => t('Brew'),
    '#collapsed' => FALSE,  
  );
  
  $form['brew']['dynamic_filtres_brew_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Brew  name'),
    '#default_value' => variable_get('dynamic_filtres_brew_name'),
    '#required' => TRUE,
    '#size' => '15'
  );

  $form['brew']['dynamic_filtres_brew'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('dynamic_filtres_brew'),
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#options' => $allowed_categories,
    '#size' => '15'
  );

  $form['weight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Weight'),
    '#collapsed' => FALSE,  
  );
  
  $form['weight']['dynamic_filtres_weight_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight of product name'),
    '#default_value' => variable_get('dynamic_filtres_weight_name'),
    '#required' => TRUE,
    '#size' => '15'
  );

  $form['weight']['dynamic_filtres_weight'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('dynamic_filtres_weight'),
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#options' => $allowed_categories,
    '#size' => '15'
  );

  $form['taste'] = array(
    '#type' => 'fieldset',
    '#title' => t('Taste'),
    '#collapsed' => FALSE,  
  );
  
  $form['taste']['dynamic_filtres_taste_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Taste of product name'),
    '#default_value' => variable_get('dynamic_filtres_taste_name'),
    '#required' => TRUE,
    '#size' => '15'
  );

  $form['taste']['dynamic_filtres_taste'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('dynamic_filtres_taste'),
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#options' => $allowed_categories,
    '#size' => '15'
  );

  $form['brands'] = array(
    '#type' => 'fieldset',
    '#title' => t('Brands'),
    '#collapsed' => FALSE,  
  );

  $form['brands']['dynamic_filtres_brands_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Brands name'),
    '#default_value' => variable_get('dynamic_filtres_brands_name'),
    '#required' => TRUE,
    '#size' => '15'
  );
  
  $form['brands']['dynamic_filtres_brands'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('dynamic_filtres_brands'),
    '#required' => TRUE,
    '#multiple' => TRUE,
    '#options' => $allowed_categories,
    '#size' => '15'
  );

  return system_settings_form($form);
}