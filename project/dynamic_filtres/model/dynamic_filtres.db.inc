<?php

function dynamic_filtres_db_get_child ($tid) {
  $query = db_select('taxonomy_term_hierarchy', 'tth');
  $query -> condition('tth.parent',$tid,'=');
  $query -> fields('tth', array('tid'));
  $allowed_categories = $query->execute()->fetchField();
  return $allowed_categories;  
}

function dynamic_filtres_db_product_types ($tid){
  $query = db_select('taxonomy_term_hierarchy', 'tth');
  $query -> condition('tth.parent',$tid,'=');
  $query -> join('field_data_field_product_display_category', 'fm', 'fm.field_product_display_category_tid = tth.tid');
  $query -> join('taxonomy_term_data', 'td', 'fm.field_product_display_category_tid = td.tid' );
  $query -> fields('td', array('tid', 'name'));
  $allowed_categories = $query->execute()->fetchAllKeyed();
  return $allowed_categories; 
}

function dynamic_filtres_db_product_mades ($tid) {
  if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_mades', 'fm', 'category.entity_id = fm.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fm.field_mades_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
  }
  else {
    $query = db_select('field_data_field_product_display_category', 'category');
    $query -> condition('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_mades', 'fm', 'category.entity_id = fm.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fm.field_mades_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
  }
  return $allowed_categories;
}

function dynamic_filtres_db_product_brew ($tid) {
  if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_brew', 'fp', 'category.entity_id = fp.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fp.field_brew_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
    return $allowed_categories;
  }
  else {
    $query = db_select ('field_data_field_product_display_category', 'category');
    $query -> condition ('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_brew', 'fp', 'category.entity_id = fp.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fp.field_brew_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
    return $allowed_categories;
  }
} 

function dynamic_filtres_db_product_packages ($tid) {
  if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_product_display_products', 'display', 'category.entity_id = display.entity_id');
    $query -> join('commerce_product', 'cp', 'display.field_product_display_products_product_id = cp.product_id'); 
    $query -> join('field_data_field_package', 'fp', 'cp.product_id = fp.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fp.field_package_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
    return $allowed_categories;
  }
  else {
    $query = db_select ('field_data_field_product_display_category', 'category');
    $query -> condition ('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_product_display_products', 'display', 'category.entity_id = display.entity_id');
    $query -> join('commerce_product', 'cp', 'display.field_product_display_products_product_id = cp.product_id'); 
    $query -> join('field_data_field_package', 'fp', 'cp.product_id = fp.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fp.field_package_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
    return $allowed_categories;
  }
}

function dynamic_filtres_db_product_taste ($tid) {
  if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_taste', 'fm', 'category.entity_id = fm.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fm.field_taste_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
    return $allowed_categories;
  }
  else {
    $query = db_select ('field_data_field_product_display_category', 'category');
    $query -> condition ('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_taste', 'fp', 'category.entity_id = fp.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fp.field_taste_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
    return $allowed_categories;
  }
}

function dynamic_filtres_db_product_brands ($tid) {
  if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_brands', 'fm', 'category.entity_id = fm.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fm.field_brands_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
  }
  else {
    $query = db_select('field_data_field_product_display_category', 'category');
    $query -> condition('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_brands', 'fm', 'category.entity_id = fm.entity_id');
    $query -> join('taxonomy_term_data', 'td', 'fm.field_brands_tid = td.tid');
    $query -> fields('td', array('tid', 'name'));
    $allowed_categories = $query -> execute() -> fetchAllKeyed();
  }
  return $allowed_categories;
}

function dynamic_filtres_db_parents ($tid) {
  $query = db_select('taxonomy_term_hierarchy', 'th');
  $query -> condition('th.tid', $tid, '=');
  $query -> fields('th', array('parent')); 
  $parent_tid = $query -> execute() -> fetchField();
  return $parent_tid; 
}

function dynamic_filtres_db_product_find_max_price ($tid) {
  if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_product_display_products', 'display', 'category.entity_id = display.entity_id');
    $query -> join('commerce_product', 'cp', 'display.field_product_display_products_product_id = cp.product_id');
    $query -> innerJoin('field_data_commerce_price', 'fp', 'fp.revision_id = cp.revision_id');
    $query -> addExpression('MAX(fp.commerce_price_amount)');
    $max = $query -> execute() -> fetchField();
    return $max;
  }
  else {
    $query = db_select ('field_data_field_product_display_category', 'category');
    $query -> condition ('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_product_display_products', 'display', 'category.entity_id = display.entity_id');
    $query -> join('commerce_product', 'cp', 'display.field_product_display_products_product_id = cp.product_id');
    $query -> innerJoin('field_data_commerce_price', 'fp', 'fp.revision_id = cp.revision_id');
    $query -> addExpression('MAX(fp.commerce_price_amount)');
    $max = $query -> execute() -> fetchField();
    return $max;
  }
}

function dynamic_filtres_db_product_find_min_price ($tid) {
 if (dynamic_filtres_db_get_child($tid) != NULL ) {
    $query = db_select('taxonomy_term_hierarchy', 'tth');
    $query -> condition('tth.parent',$tid,'=');
    $query -> join('field_data_field_product_display_category', 'category', 'category.field_product_display_category_tid = tth.tid');
    $query -> join('field_data_field_product_display_products', 'display', 'category.entity_id = display.entity_id');
    $query -> join('commerce_product', 'cp', 'display.field_product_display_products_product_id = cp.product_id');
    $query -> innerJoin('field_data_commerce_price', 'fp', 'fp.revision_id = cp.revision_id');
    $query -> addExpression('MIN(fp.commerce_price_amount)');
    $min = $query -> execute() -> fetchField();
    return $min;
  }
  else {
    $query = db_select ('field_data_field_product_display_category', 'category');
    $query -> condition ('category.field_product_display_category_tid',$tid,'=');
    $query -> join('field_data_field_product_display_products', 'display', 'category.entity_id = display.entity_id');
    $query -> join('commerce_product', 'cp', 'display.field_product_display_products_product_id = cp.product_id');
    $query -> innerJoin('field_data_commerce_price', 'fp', 'fp.revision_id = cp.revision_id');
    $query -> addExpression('MIN(fp.commerce_price_amount)');
    $min = $query -> execute() -> fetchField();
    return $min;
  }
}

function dynamic_filtres_db_product_find_categories () {
  $query = db_select('taxonomy_term_data', 'ttd') ;
  $query -> join ('taxonomy_term_hierarchy', 'tth', 'ttd.tid = tth.tid');
  $query -> condition ('tth.parent', 0);
  $query -> condition ('ttd.vid', 2);
  $query->fields('ttd', array('tid', 'name'));
  $allowed_categories = $query->execute()->fetchAllKeyed();
  return $allowed_categories; 
}

function dynamic_filtres_db_block_change_region ($region) {
  $query = db_update('block') ;
  $query->fields(array(
  'region' => $region,
  ));
  $query->condition('delta', '-exp-shop3-page');
  $num_updated = $query->execute();
}
