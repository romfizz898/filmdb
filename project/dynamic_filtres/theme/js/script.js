(function ($) {
  Drupal.behaviors.dynamic_filtres = {
    attach: function (context, settings) {
            
      var num = 0;    
      if (settings.category === '0') {
        $('#edit-tid-category-wrapper').hide();
        num++;
      }
    
      if (settings.mades === '0') {
        $('#edit-tid-category-wrapper').hide();
        num++;
      }

      if (settings.taste === '0') {
        $('#edit-field-taste-tid-wrapper').hide();
        num++;
      }

      if (settings.package === '0') {
       $('#edit-field-package-tid-wrapper').hide();
       num++;
      }

      if (settings.brew === '0') {
        $('#edit-field-brew-tid-wrapper').hide();
        num++;
      }

      if (settings.brands === '0') {
        $('#edit-field-brands-tid-wrapper').hide();
        num++;
      }

      var width = $('.views-exposed-widgets.clearfix').width();
      if (num < 2) {
        num = 7 - num;
        width = width / num; 
      }
      else {
        width = 150;
      }

      $('.views-exposed-widgets.clearfix').children().css({'width':width + 'px'});
  
      $('#edit-commerce-price-amount-wrapper .views-widget').prepend('<div class="container" style="overflow:visible; width:'+ width +'px" > </div>');
      $('.container').prepend('<div class="box" style="white-space:nowrap" > </div>');
      $('.box').append($('.form-item-commerce-price-amount-min'));
      $('.box').append($('.form-item-commerce-price-amount-max'));
      $('.box').append($('.bef-slider')); 
      $('.bef-slider').css({'width': width - 20 + 'px'});
      $('.box').append($('.views-exposed-widget.views-submit-button'));
    
      $("label[for='edit-commerce-price-amount']").addClass('dynamic_filter_label');

      $('.form-item-commerce-price-amount-min').css({'width': width/2 - 10});

      $('.form-item-commerce-price-amount-min').addClass('dynamic_filter_price');
    
      $('.form-item-commerce-price-amount-max').css({'width': width/2 - 10});

      $('.form-item-commerce-price-amount-max').addClass('dynamic_filter_price');
    
      $('.form-item-commerce-price-amount-max>label').hide();
    
      $('.views-exposed-widget.views-submit-button').addClass('dynamic_filter_inline');
    
      $('.bef-checkboxes').each(function() { 
        $path = $(this).parent().parent('.form-item').parent().parent().attr('id'); 
        $("label[for='"+ $path.replace('-wrapper','') +"']").addClass('dynamic_filter_label');

        if ($(this).children('div').length > 5) {
          $(this).append('<div class="more" style="position:relative" title="' + Drupal.t('Еще!') + '"> Еще </a>');

          if ($(this).children('div').length > 11 ) {
            $(this).children('.more').append('<div class="filter_values" style=" height:150px; overflow: auto;"> </div>');
          }
          else {
            $(this).children('.more').append('<div class="filter_values"> </div>'); 
          }
          $(this).children('.form-item').each(function(i,elem) {
            if (i > 4 ) {
              $(this).parent().children('.more').children('.filter_values').append($(this));
            }
          }) 
        
          $('#' + $(this).parent('.form-checkboxes').parent('.form-item').parent().parent().attr('id') + ' .more').mouseenter(function(e) {
            $(this).children('.filter_values').show();

            $(this).parent().mouseleave(function(e){
              $(this).children('.more').children('.filter_values').hide()
            });
          }); 
        }     
      });
    
      $('.filter_values').each(function() {
        $(this).children('.form-item.highlight').each(function() {
          $(this).parent().parent().parent().prepend($(this));
        })    
      })
    }
  };
}(jQuery));